package semo.etl;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SemoETLApplication {

    public static void main(String[] args) {
        SpringApplication.run(SemoETLApplication.class, args);
    }
}
