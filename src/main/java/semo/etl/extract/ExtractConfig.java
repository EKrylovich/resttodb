package semo.etl.extract;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
import semo.etl.extract.rest.RestApiItemReader;

@Configuration
public class ExtractConfig {
    @Bean
    public RestApiItemReader restApiReader(final RestTemplate restTemplate){
        return new RestApiItemReader("https://jsonplaceholder.typicode.com/posts", restTemplate);
    }

    @Bean
    public RestTemplate restTemplate(){
        return new RestTemplate();
    }
}
