package semo.etl.extract.rest.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import semo.etl.model.Post;

public class PostDto {
    private final String id;
    private final String title;
    private final String body;

    @JsonCreator
    public PostDto(final @JsonProperty(value = "id") String id,
                   final @JsonProperty(value = "title") String title,
                   final @JsonProperty(value = "body") String body) {
        this.id = id;
        this.title = title;
        this.body = body;
    }

    public Post createModel(){
        return new Post(title, body);
    }
}
