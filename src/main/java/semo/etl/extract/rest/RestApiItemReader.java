package semo.etl.extract.rest;

import org.springframework.batch.item.ItemReader;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import semo.etl.extract.rest.dto.PostDto;

import java.util.Arrays;
import java.util.List;


public class RestApiItemReader implements ItemReader<PostDto> {
    private final String apiUrl;
    private final RestTemplate restTemplate;

    private int nextStudentIndex;
    private List<PostDto> postData;

    public RestApiItemReader(final String apiUrl, final RestTemplate restTemplate) {
        this.apiUrl = apiUrl;
        this.restTemplate = restTemplate;
        nextStudentIndex = 0;
    }

    @Override
    public PostDto read() {
        initialize();
        if (nextStudentIndex < postData.size()) {
            final PostDto nextPost = postData.get(nextStudentIndex);
            nextStudentIndex++;
            return nextPost;
        }
        return null;
    }

    private void initialize() {
        if (studentDataIsNotInitialized()) {
            postData = fetchStudentDataFromAPI();
        }
    }

    private boolean studentDataIsNotInitialized() {
        return this.postData == null;
    }

    private List<PostDto> fetchStudentDataFromAPI() {
        ResponseEntity<PostDto[]> response = restTemplate.getForEntity(
                apiUrl,
                PostDto[].class
        );
        PostDto[] studentData = response.getBody();
        return Arrays.asList(studentData);
    }
}
