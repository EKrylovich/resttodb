package semo.etl.transform;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;
import semo.etl.extract.rest.dto.PostDto;
import semo.etl.model.Post;

public class PostDataProcessor implements ItemProcessor<PostDto, Post> {

    @Override
    public Post process(final PostDto postDto) {
        return postDto.createModel();
    }
}
