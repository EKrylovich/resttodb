package semo.etl.transform;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TransformConfig {
    @Bean
    public PostDataProcessor processor() {
        return new PostDataProcessor();
    }
}
