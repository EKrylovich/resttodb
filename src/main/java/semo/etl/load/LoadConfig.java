package semo.etl.load;

import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import semo.etl.model.Post;

import javax.sql.DataSource;

@Configuration
public class LoadConfig {

    @Bean
    public JdbcBatchItemWriter<Post> writer(final DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<Post>()
                .itemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>())
                .sql("INSERT INTO post (title, body) VALUES (:title, :body)")
                .dataSource(dataSource)
                .build();
    }
}
