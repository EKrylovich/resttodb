package semo.etl.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import semo.etl.extract.ExtractConfig;
import semo.etl.extract.rest.RestApiItemReader;
import semo.etl.extract.rest.dto.PostDto;
import semo.etl.listeners.JobCompletionNotificationListener;
import semo.etl.load.LoadConfig;
import semo.etl.model.Post;
import semo.etl.transform.PostDataProcessor;
import semo.etl.transform.TransformConfig;

@Configuration
@EnableBatchProcessing
@Import({ExtractConfig.class, TransformConfig.class, LoadConfig.class})
public class RestToDBConfiguration {
    @Autowired
    public JobBuilderFactory jobBuilderFactory;

    @Autowired
    public StepBuilderFactory stepBuilderFactory;

    @Bean
    public Job importUserJob(final JobCompletionNotificationListener listener,
                             final Step etl) {
        return jobBuilderFactory.get("importPostsJob")
                .incrementer(new RunIdIncrementer())
                .listener(listener)
                .flow(etl)
                .end()
                .build();
    }

    @Bean
    public Step etl(final RestApiItemReader apiReader,
                    final PostDataProcessor processor,
                    final JdbcBatchItemWriter<Post> writer) {
        return stepBuilderFactory.get("etl")
                .<PostDto, Post> chunk(10)
                .reader(apiReader)
                .processor(processor)
                .writer(writer)
                .build();
    }
}
